/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan05b;

/**
 *
 * @author Gadget House
 */
public class Mhs {
    //property........................
    private String nama;
    private float ipk;
    private String kodeMakul;
    private String asalkota;
    private int jumlahSks;
    
    //behavior........................
    public void setNama(String nama){
        this.nama = nama;
    }
    public void setIpk(int ipk){
        this.ipk = ipk;
    }
    public String getNama(){
        return this.nama;
    }
    public float getIpk() {
        return this.ipk;
    }

    public String getKodeMakul() {
        return kodeMakul;
    }

    public void setKodeMakul(String kodeMakul) {
        this.kodeMakul = kodeMakul;
    }

    public String getAsalkota() {
        return asalkota;
    }

    public void setAsalkota(String asalkota) {
        this.asalkota = asalkota;
    }

    public int getJumlahSks() {
        return jumlahSks;
    }

    public void setJumlahSks(int jumlahSks) {
        this.jumlahSks = jumlahSks;
    }
}
